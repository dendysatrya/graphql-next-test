import { gql } from "@apollo/client";
import { client } from "./getApi";

export const getAllRockets = async () => {
  const { data } = await client.query({
    query: gql`
      query GetRockets {
        rockets {
          id
          engines {
            number
            type
            version
            layout
            engine_loss_max
            propellant_1
            propellant_2
            thrust_to_weight
          }
          diameter {
            feet
            meters
          }
          active
          boosters
          name
          wikipedia
          cost_per_launch
          description
          first_flight
        }
      }
    `,
  });
  return data.rockets;
};

export const getAllMissions = async () => {
  const { data } = await client.query({
    query: gql`
      query GetMissions {
        missions {
          description
          id
          manufacturers
          name
        }
      }
    `,
  });
  return data.missions;
};

export const searchMissions = async (name) => {
  const { data } = await client.query({
    query: gql`
          query GetMissions {
            missions(find: {name: "${name}"}) {
                description
                id
                manufacturers
                name
              }
          }
        `,
  });
  return data.missions;
};
