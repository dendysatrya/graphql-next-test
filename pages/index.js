import Head from "next/head";
import Link from "next/link";
import ContainerDark from "../components/containerDark";
import styles from "../styles/Home.module.scss";

export default function Home() {
  return (
    <>
      <Head>
        <title>Welcome to Space X Database</title>
        <meta property="og:title" content="Space X Database" key="title" />
      </Head>
      <ContainerDark>
        <div className={styles.headerTextContainer}>
          <h1 className={styles.welcomeText}>Welcome to</h1>
          <h1 className={styles.subwelcomeText}>Space X Database</h1>
        </div>
        <div className={styles.overviewTextContainer}>
          <h3 className={styles.overviewText}>
            Discover all the interesting details about capsules, cores,
            landpads, launches, missions, payloads, rockets, ships & much more.
            You could even check if Elon’s Roadster has finally arrived to Mars!
            🚀
          </h3>
        </div>
        <Link href="/discover/rockets">
          <a>
            <button className={styles.buttonGo}>Let's get into it!</button>
          </a>
        </Link>
      </ContainerDark>
    </>
  );
}
