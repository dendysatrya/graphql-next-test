import { useEffect, useState } from "react";
import Container from "../../components/container";
import MenuBar from "../../components/menuBar";
import MissionsCard from "../../components/missionsCard";
import { getAllMissions, searchMissions } from "../../utils/queryFetch";
import styles from "../../styles/Missions.module.scss";
import Spinner from "../../components/spinner";
import Head from "next/head";

export default function Missions({ missions }) {
  const [searchVal, setSearchVal] = useState("");
  const [missionsData, setMissionsData] = useState(missions);
  const [loadingData, setLoadingData] = useState(false);

  useEffect(async () => {
    if (searchVal.length > 2) {
      setLoadingData(true);
      const newMissionsData = await searchMissions(searchVal);
      setMissionsData(newMissionsData);
      setLoadingData(false);
    } else {
      setMissionsData(missions);
    }
  }, [searchVal]);

  return (
    <>
      <Head>
        <title>Space X Missions 📚</title>
        <meta property="og:title" content="Space X Missions" key="title" />
      </Head>
      <Container>
        <h1>Space X Missions</h1>
        <MenuBar />
        <input
          type="text"
          placeholder="Search for mission's name"
          className={styles.searchInput}
          value={searchVal}
          onChange={(e) => setSearchVal(e.target.value)}
        />
        {loadingData ? (
          <Spinner />
        ) : missionsData && missionsData.length ? (
          missionsData.map((mission) => (
            <MissionsCard mission={mission} key={mission.id} />
          ))
        ) : (
          <p>Missions not found.</p>
        )}
      </Container>
    </>
  );
}

export async function getStaticProps() {
  const missions = await getAllMissions();
  return { props: { missions } };
}
