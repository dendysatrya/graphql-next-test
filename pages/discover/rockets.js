import Container from "../../components/container";
import RocketCard from "../../components/rocketCard";
import { getAllRockets } from "../../utils/queryFetch";
import MenuBar from "../../components/menuBar";
import Head from "next/head";

export default function Rockets({ rockets }) {
  return (
    <>
      <Head>
        <title>Space X Rockets 🚀</title>
        <meta property="og:title" content="Space X Rockets" key="title" />
      </Head>
      <Container>
        <h1>Space X Rockets</h1>
        <MenuBar />
        {rockets ? (
          rockets.map((rocket) => (
            <RocketCard rocket={rocket} key={rocket.id} />
          ))
        ) : (
          <p>No rocket found.</p>
        )}
      </Container>
    </>
  );
}

export async function getStaticProps() {
  const rockets = await getAllRockets();
  return { props: { rockets } };
}
