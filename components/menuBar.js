import styles from "../styles/MenuBar.module.scss";
import Link from "next/link";
import { useRouter } from "next/router";

export default function MenuBar() {
  const router = useRouter();
  return (
    <div className={styles.menuContainer}>
      <Link href="/discover/rockets">
        <a
          className={
            router.pathname === "/discover/rockets" ? styles.active : ""
          }
        >
          Rockets 🚀
        </a>
      </Link>
      <Link href="/discover/missions">
        <a
          className={
            router.pathname === "/discover/missions" ? styles.active : ""
          }
        >
          Missions 📚
        </a>
      </Link>
    </div>
  );
}
