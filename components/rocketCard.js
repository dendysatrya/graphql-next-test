import styles from "../styles/RocketCard.module.scss";
import Link from "next/link";
import Moment from "react-moment";

export default function RocketCard({ rocket }) {
  const {
    name,
    id,
    engines,
    diameter,
    cost_per_launch,
    wikipedia,
    active,
    description,
    boosters,
    first_flight,
  } = rocket;
  return (
    <div className={styles.baseCard}>
      <div className={styles.rocketNameContainer}>
        <div className={styles.leftSide}>
          <h1 className={styles.rocketName}>{name}</h1>
          <p className={styles.rocketId}>(code: {id})</p>
          {active ? (
            <div className={styles.activeRocket}>Active</div>
          ) : (
            <div className={styles.inactiveRocket}>Inactive</div>
          )}
        </div>
        <div className={styles.rightSide}>
          First launched at{" "}
          <Moment format="MMMM Do YYYY">{first_flight}</Moment>
        </div>
      </div>
      <div className={styles.rocketOverview}>
        <div className={styles.description}>
          <h3 className={styles.overviewTitle}>Description</h3>
          <p className={styles.descriptionText}>{description}</p>
        </div>
        <div className={styles.rowOverview}>
          <div className={styles.engineDetails}>
            <h3 className={styles.overviewTitle}>Engine Details</h3>
            <table>
              <tbody>
                <tr>
                  <td>Number</td>
                  <td>:</td>
                  <td>{engines.number || "-"}</td>
                </tr>
                <tr>
                  <td>Type</td>
                  <td>:</td>
                  <td>{engines.type || "-"}</td>
                </tr>
                <tr>
                  <td>Version</td>
                  <td>:</td>
                  <td>{engines.version || "-"}</td>
                </tr>
                <tr>
                  <td>Layout</td>
                  <td>:</td>
                  <td>{engines.layout || "-"}</td>
                </tr>
                <tr>
                  <td>Engine Loss Max</td>
                  <td>:</td>
                  <td>{engines.engine_loss_max || "-"}</td>
                </tr>
                <tr>
                  <td>Propellant</td>
                  <td>:</td>
                  <td>
                    {engines.propellant_1} & {engines.propellant_2}
                  </td>
                </tr>
                <tr>
                  <td>Thrust to Weight</td>
                  <td>:</td>
                  <td>{engines.thrust_to_weight || "-"}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className={styles.col}>
            <div className={styles.diameterDetails}>
              <h3 className={styles.overviewTitle}>Diameter</h3>
              <p>
                {diameter.feet} feet <i>({diameter.meters} meters)</i>
              </p>
            </div>
            <div className={styles.boostersDetails}>
              <h3 className={styles.overviewTitle}>Boosters</h3>
              <p>
                {boosters ? (
                  `${boosters} boosters`
                ) : (
                  <i>Don't have any booster.</i>
                )}
              </p>
            </div>
          </div>
          <div className={styles.col}>
            <div className={styles.costDetails}>
              <h3 className={styles.overviewTitle}>Costs per Launch</h3>
              <p>
                {cost_per_launch.toLocaleString("en-US", {
                  style: "currency",
                  currency: "USD",
                })}
              </p>
            </div>
            <div className={styles.wikipediaLink}>
              <h3 className={styles.overviewTitle}>Wikipedia</h3>
              {wikipedia ? (
                <Link href={wikipedia}>
                  <a>Check on Wikipedia.</a>
                </Link>
              ) : (
                <p>-</p>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
