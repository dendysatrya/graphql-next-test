import styles from "../styles/ContainerDark.module.scss";

export default function ContainerDark({ children }) {
  return <div className={styles.container}>{children}</div>;
}
