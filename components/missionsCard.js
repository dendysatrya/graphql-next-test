import styles from "../styles/MissionsCard.module.scss";

export default function MissionsCard({ mission }) {
  const { name, id, description, manufacturers } = mission;
  return (
    <div className={styles.baseCard}>
      <div className={styles.missionNameContainer}>
        <h1 className={styles.missionName}>{name}</h1>
        <p className={styles.missionId}>(code: {id})</p>
      </div>
      <div className={styles.missionOverview}>
        <h3>Mission Detail</h3>
        <p>{description}</p>
        <h3>Manufacturers</h3>
        <p>
          {manufacturers.map((manufacturer, index) =>
            index === 0 ? `${manufacturer}` : `, ${manufacturer}`
          )}
        </p>
      </div>
    </div>
  );
}
